include docker.mk

.PHONY: test

DRUPAL_VER ?= 7.78
PHP_VER ?= 7.4

test:
	cd ./$(DRUPAL_VER) && PHP_VER=$(PHP_VER) ./run.sh
