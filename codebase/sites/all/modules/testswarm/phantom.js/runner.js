/*
 * QtWebKit-powered headless test runner using PhantomJS
 *
 * PhantomJS binaries: http://phantomjs.org/download.html
 * Requires PhantomJS 1.6+ (1.7+ recommended)
 *
 * Run with:
 *   phantomjs runner.js [url-of-your-qunit-testsuite]
 *
 * e.g.
 *   phantomjs runner.js http://localhost/qunit/test/index.html
 */

/*jshint latedef:false */
/*global phantom:false, require:false, console:false, window:false, QUnit:false */

(function() {
	'use strict';

	var args = require('system').args;

	// arg[0]: scriptName, args[1...]: arguments
	if (args.length !== 2) {
		console.error('Usage:\n  phantomjs runner.js [url-of-your-qunit-testsuite]');
		phantom.exit(1);
	}

	var url = args[1],
    links = [],
    page = require('webpage').create();

  console.log(url.match(/:\/\/(www\.)?(.[^/:]+)/)[2]);
  phantom.addCookie({
    'name': 'karma',
    'value': 'phantomjs',
    'domain': url.match(/:\/\/(www\.)?(.[^/:]+)/)[2]
  });

  page.open(url, function () {
    var jsonSource = page.plainText;
    links = JSON.parse(jsonSource);
    page.release();
    processLinks();
  });

  function processLinks() {
    if (links.length == 0) {
      phantom.exit();
    }
    else {
      var link = links.pop();
      console.log(link);
      page = require('webpage').create();
      page.onClosing = function(closingPage) {
        console.log('URL: ' + closingPage.url);
        var Q = closingPage.evaluate(function() {return document.QunitResults;});
        if (Q) {
          var result = Q;
          console.log('Took ' + result.runtime +  'ms to run ' + result.total + ' tests. ' + result.passed + ' passed, ' + result.failed + ' failed.');
        }
      };
      page.open(link, onOpen);
    }
  }

  function onOpen(status) {
		if (status !== 'success') {
			console.error('Unable to access network: ' + status);
		} else {
			var qunitMissing = page.evaluate(function() { return (typeof QUnit === 'undefined' || !QUnit); });
			if (qunitMissing) {
				console.error('The `QUnit` object is not present on this page.');
			}
		}
    page.close();
    page.release();
    processLinks();
  }

	page.onConsoleMessage = function(msg) {
		console.log(msg);
	};

})();
