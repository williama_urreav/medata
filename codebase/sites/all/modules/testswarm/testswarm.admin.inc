<?php

/**
 * @file
 *   TestSwarm module - admin.
 */

/**
 * Clear tests.
 */

function testswarm_clear_test_details($form, $form_state, $caller) {
  $tests = testswarm_defined_tests();
  if (!empty($tests) && (isset($tests[$caller]) || $caller == 'all')) {
    return confirm_form(
      array(
        'caller' => array(
          '#type' => 'value',
          '#value' => $caller,
        ),
      ),
      $caller == 'all' ? t('Are you sure you want to remove all test details?') : t('Are you sure you want to remove all test details of %caller?', array('%caller' => $caller)),
      'testswarm-tests',
      t('This action cannot be undone.'),
      t('Remove test details'),
      t('Cancel')
    );
  }
}


/**
 * Form submission handler for testswarm_clear_test_details().
 */
function testswarm_clear_test_details_submit($form, &$form_state) {
  $caller = $form_state['values']['caller'];
  testswarm_test_delete($caller);
  if (variable_get('testswarm_save_results_remote', 0)) {
    $result = xmlrpc(
      variable_get('testswarm_save_results_remote_url', ''),
      array(
        'testswarm.test.delete' => array(
          $caller,
          REQUEST_TIME,
          testswarm_xmlrpc_get_hash(),
        ),
      )
    );
    if (!$result) {
      $error = xmlrpc_error();
      if ($error) {
        watchdog('testswarm_xmlrpc', $error->code . ': ' . check_plain($error->message));
      }
      else {
        watchdog('testswarm_xmlrpc', 'Something went wrong deleting the result from the remote server');
      }
    }
  }
  $form_state['redirect'] = 'testswarm-tests';
}

/**
 * Form callback for admin settings.
 */
function testswarm_admin_settings_form($form, $form_state) {
  $form['browserstack'] = array(
    '#type' => 'fieldset',
    '#title' => t('Browserstack'),
    '#description' => t('Browserstack settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['browserstack']['testswarm_browserstack_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('testswarm_browserstack_username', ''),
    '#description' => t('Your browserstack username'),
  );
  $form['browserstack']['testswarm_browserstack_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('testswarm_browserstack_password', ''),
    '#description' => t('Your browserstack password'),
  );
  $form['browserstack']['testswarm_browserstack_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API URL'),
    '#default_value' => variable_get('testswarm_browserstack_api_url', 'http://api.browserstack.com/1'),
    '#description' => t('The browserstack url all requests are made to. From the browserstack documentation: "All requests are made to http://api.browserstack.com/VERSION"')
  );
  $form['testswarm_remote_storage'] = array(
    '#type' => 'fieldset',
    '#title' => t('Remote storage'),
    '#description' => t('Remote storage settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['testswarm_remote_storage']['testswarm_save_results_remote'] = array(
    '#type' => 'checkbox',
    '#title' => t('Save a copy of the test results on a remote server'),
    '#default_value' => variable_get('testswarm_save_results_remote', 0),
  );
  $form['testswarm_remote_storage']['testswarm_save_results_remote_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint URL'),
    '#description' => t('An absolute URL of the XML-RPC endpoint.'),
    '#default_value' => variable_get('testswarm_save_results_remote_url', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="testswarm_save_results_remote"]' => array('checked' => TRUE),
      ),
      'required' => array(
        ':input[name="testswarm_save_results_remote"]' => array('checked' => TRUE),
      ),
    )
  );
  $form['testswarm_remote_storage']['testswarm_generate_secret'] = array(
    '#type' => 'radios',
    '#title' => t('Create a new shared key'),
    '#options' => array(
      0 => t('Manually'),
      1 => t('Generate new key'),
    ),
    '#default_value' => 0,
  );
  $form['testswarm_remote_storage']['testswarm_shared_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Shared key'),
    '#description' => t('This key is used to counteract malicious calls to the remote server. It needs to be the same on the client and the server.'),
    '#default_value' => variable_get('testswarm_shared_secret', 'S-gbyyUjnOh76A2nMJFYHXJKwqC5zsDfAw-3q65aVB4'),
    '#states' => array(
      'visible' => array(
        ':input[name="testswarm_generate_secret"]' => array('value' => 0),
      ),
    ),
  );
  $form =  system_settings_form($form);
  array_unshift($form['#submit'], 'testswarm_admin_settings_form_submit');
  return $form;
}

function testswarm_admin_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['testswarm_save_results_remote']) {
    if (empty($values['testswarm_save_results_remote_url'])) {
      form_set_error('testswarm_save_results_remote_url', t('Endpoint URL is required.'));
    }
    if (!valid_url($values['testswarm_save_results_remote_url'], TRUE)) {
      form_set_error('testswarm_save_results_remote_url', t('Endpoint URL must be a valid URL.'));
    }
  }
}

function testswarm_admin_settings_form_submit($form, &$form_state) {
  $values = &$form_state['values'];
  if ($values['testswarm_generate_secret']) {
    $values['testswarm_shared_secret'] = drupal_hash_base64(drupal_random_bytes(55));
  }
}
