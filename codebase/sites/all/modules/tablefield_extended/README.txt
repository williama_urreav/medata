TableField Extended
----------
This module provides a simple, generic form/widget to input tabular data. The
form allows the user to select the number of rows/columns in the table, then
enter the data via textfields. Since this is a field, it is automatically
revision capable, multi-value capable, and has integration with Views.

INSTALLATION
------------
- Copy tablefield_extended directory to /sites/all/modules
- Enable module at /admin/modules
- Add a tablefield_extended to any entity, for example /admin/structure/types
