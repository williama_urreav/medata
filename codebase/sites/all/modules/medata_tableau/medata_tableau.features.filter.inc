<?php

/**
 * @file
 * medata_tableau.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function medata_tableau_filter_default_formats() {
  $formats = array();

  // Exported format: Tableau Embed.
  $formats['tableau_embed'] = array(
    'format' => 'tableau_embed',
    'name' => 'Tableau Embed',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(),
  );

  return $formats;
}
