<?php

/**
 * @file
 * Migration File for DKAN Harvest Data.Json.
 */

use League\Csv\Reader;
use function League\Csv\delimiter_detect;

include_once drupal_get_path('module', 'dkan_harvest_datajson') . '/dkan_harvest_datajson.migrate.inc';

define(MEDATA_HARVEST_DATAJSON_DESTINATION_PATH, "public://medata_harvest_files");

/**
 * Migration class to harves POD endpoints.
 */
class MedataDatajsonHarvestMigration extends DatajsonHarvestMigration {
  const DATASTORE_NO = 0;
  const DATASTORE_SKIP = 1;
  const DATASTORE_SLOW = 2;
  const DATASTORE_FAST = 3;

  /**
   * @Override parent::setFieldMappings()
   *
   * Add support for custom author and granularity fields
   */
  public function setFieldMappings($version = '1.1') {
    parent::setFieldMappings($version);

    $this->addFieldMapping('field_author', 'medataAuthor');
    $this->addFieldMapping('field_granularity', 'medataGranularity');
  }

  /**
   * @Override parent::prepareRowResources()
   */
  public function prepareRowResources($resources_row_data) {
    $resources = array();

    foreach ($resources_row_data as $resource_row_data) {
      // Fallback to the accessURL if the downloadURL is not available.
      $url = $resource_row_data->downloadURL;
      if (empty($url)) {
        $url = $resource_row_data->accessURL;
      }

      $resource = $this->prepareResourceHelper($url,
        $resource_row_data->format,
        $resource_row_data->title,
        NULL,
        $resource_row_data->description);

      if ($resource) {
        // Add raw distribution object to the resource for later reuse.
        $resource->raw = $resource_row_data;
        $resources[] = $resource;
      }
    }

    return $resources;
  }

  /**
   * {@inheritdoc}
   */
  protected function createResources(&$dataset, stdClass &$row) {
    // Delete datastore instance if applicable.
    $dataset_old_emw = entity_metadata_wrapper('node', $dataset->nid);
    $field_resources_uuids = array();
    if (isset($dataset_old_emw->field_resources)) {
      foreach ($dataset_old_emw->field_resources->getIterator() as $delta => $resource_emw) {
        $field_resources_uuids[] = $resource_emw->uuid->value();
      }
    }

    if (!empty($field_resources_uuids)) {
      foreach ($field_resources_uuids as $uuid) {
        $datastore = dkan_datastore_go($uuid);
        if (empty($datastore)) {
          continue;
        }

        $table_name = $datastore->tableName;

        $table = data_get_table($table_name);
        if ($table) {
          $table->drop();
        }
        elseif (db_table_exists($table_name)) {
          db_drop_table($table_name);
        }
      }
    }

    parent::createResources($dataset, $row);
  }

  /**
   * @Override parent::createResourceNode()
   *
   * Save to local a file instead of referencing the remote URL.
   */
  public function createResourceNode(stdClass $res) {
    migrate_instrument_start('MedataDatajsonHarvestMigration->createResourceNode');
    // Creates a new resource for every linked file.
    // Linked files contain title, format, and accessURL.
    $values = array(
      'type' => 'resource',
      'changed' => $res->created,
      'is_new' => TRUE,
    // TODO check this.
      "language" => LANGUAGE_NONE,
      "uid" => 1,
      "status" => NODE_PUBLISHED,
    );

    $resourceEMW = entity_create('node', $values);
    module_invoke_all('node_prepare', $resourceEMW);

    // All datastore enabled resources will be added to the queue. Suppress
    // import on the feed side.
    $resourceEMW->feeds['suppress_import'] = TRUE;

    $resourceEMW = entity_metadata_wrapper('node', $resourceEMW);

    $resourceEMW->title = $res->title;
    $resourceEMW->body->format->set('html');
    $resourceEMW->body->value->set($res->description);

    // Process field_format.
    if (isset($res->format)) {
      $vocab = 'format';
      $term = $this->createTax($res->format, $vocab);

      if (is_null($term)) {
        // Something went left. Report.
        $message = t(
        "Cannot get taxonomy @taxonomy (@vocabulary vocabulary).",
        array(
          '@taxonomy' => $res->format,
          '@vocabulary' => $vocab,
        )
          );
        $this->saveMessage($message);
      }
      else {
        $resourceEMW->field_format = $term->tid;
      }
    }

    $datastore_queue = self::DATASTORE_NO;

    if ($res->url_type == 'file') {
      /**
       * Process field_upload field.
       */
      $downloaded = $this->downloadRemoteFile($resourceEMW, $res);

      // Check if we should enable the datastore.
      if (isset($res->format) && $res->format == 'csv') {
        if ($downloaded == TRUE) {
          // Get Datastore config and queue hint.
          list($datastore_queue, $sourceConfigUpdate) = $this->getResourceDatastoreConfig($resourceEMW, $res);
          $importer_ids = feeds_get_importer_ids($resourceEMW->getBundle());

          if (!empty($sourceConfigUpdate) && !empty($importer_ids)) {
            $resource = $resourceEMW->value();

            foreach ($importer_ids as $importer_id) {
              $resource->feeds[$importer_id] = array_replace_recursive($resource->feeds[$importer_id], $sourceConfigUpdate);
            }

            // Reset the metadata wrapper with the new base node object.
            $resourceEMW = entity_metadata_wrapper('node', $resource);

            if ($datastore_queue == self::DATASTORE_NO
            || $datastore_queue == self::DATASTORE_SKIP) {
              $resourceEMW->field_datastore_status->set(DKAN_DATASTORE_WRONG_TYPE);
            }
            else {
              // Mark the resource as added to the datastore.
              $resourceEMW->field_datastore_status->set(DKAN_DATASTORE_FILE_EXISTS);
            }
          }
        }

        /**
         * Data Dictionaries.
         */
        if (!empty($res->raw->describedByType) && !empty($res->raw->describedBy)) {
          // If the "describedByType" is "application/schema+json", assume table schema.
          if ($res->raw->describedByType == "application/schema+json") {

            $schema = NULL;

            $context = stream_context_create(
            array(
              'http' => array('timeout' => 36000),
              'https' => array('timeout' => 36000),
            )
              );

            $schema = @file_get_contents($res->raw->describedBy, 0, $context);

            if (empty($schema)) {
              $message = t("Failed to get \"describedBy\" content from @describedBy_url",
              array('@describedBy_url' => $res->raw->describedBy));
              $this->saveMessage($message, MigrationBase::MESSAGE_ERROR);
            }
            else {
              // Sometimes the json from source will be reddiled with bad controle
              // characters. Clean those.
              $schema = preg_replace('/[[:cntrl:]]/', '', $schema);

              // Field in DB expects UTF-8, Usually Hadoop speaks in ISO-8859-1.
              // Convert if needed.
              if (!mb_check_encoding($schema, 'UTF-8')) {
                $schema = utf8_encode($schema);
              }

              list($schema_valid, $schema_errors) = dkan_data_dictionary_validate('tableschemavalidator', $schema);

              if ($schema_valid == TRUE) {
                $resourceEMW->field_describedby_schema = $schema;
                $resourceEMW->field_describedby_spec->set('tableschemavalidator');
              }
              else {
                $message = t("Data Dictionary validation failed: @schema", array('@schema' => $schema));
                $this->saveMessage($message);
                $message = t("Data Dictionary error(s): @errors", array('@errors' => implode('; ', $schema_errors)));
                $this->saveMessage($message);
              }
            }
          }
        }
      }
    }
    else {
      // Log error is a url that should be a file (csv) but it's not.
      if ($res->format == 'csv') {
        $message = t("File of type URL but with format CSV.");
        $this->saveMessage($message, MigrationBase::MESSAGE_ERROR);
      }
      // Manage API type resources.
      $resourceEMW->field_link_api->url->set($res->url);
    }

    $resourceEMW->save();

    if ($datastore_queue != self::DATASTORE_NO) {
      $success = NULL;

      switch ($datastore_queue) {
        case self::DATASTORE_SKIP:
          break;

        case self::DATASTORE_SLOW:
          $item = array(
            'uuid' => $resourceEMW->uuid->value(),
          );
          $success = DrupalQueue::get('dkan_datastore_queue')
            ->createItem($item);
          break;

        case self::DATASTORE_FAST:
          // Add the resource of type CSV to the datastore
          // import queue if CSV.
          $importer_id = 'dkan_file';
          $source = feeds_source($importer_id, $resourceEMW->getIdentifier());
          $table = feeds_flatstore_processor_table($source, array());
          $config = $sourceConfigUpdate['FeedsCSVParser'];
          $item = array(
            'source' => $source,
            'node' => $resourceEMW,
            'table' => $table,
            'config' => $config,
          );

          $success = DrupalQueue::get(dkan_datastore_fast_import_queue_name())
            ->createItem($item);
          break;
      }

      if ($success) {
        $message = t('resource "%uuid" ADDED to queue for import into datastore', array('%uuid' => $resourceEMW->uuid->value()));
        $this->saveMessage($message, MigrationBase::MESSAGE_INFORMATIONAL);
      }
      else {
        $message = t('Resource "%uuid" NOT ADDED to queue for import into datastore', array('%uuid' => $resourceEMW->uuid->value()));
        $this->saveMessage($message);
      }
    }

    migrate_instrument_stop('MedataDatajsonHarvestMigration->createResourceNode');
    return $resourceEMW->value();
  }

  /**
   * {@inheritdoc}
   */
  public function complete($dataset, $row) {
    migrate_instrument_start('MedataDatajsonHarvestMigration->complete');
    parent::complete($dataset, $row);

    $datasetEMW = entity_metadata_wrapper('node', $dataset);

    // Update the dataset topic from source if the taxonomy term is available.
    if (!empty($row->theme)) {
      $topics = array();

      $themes_matched = array();

      foreach ($row->theme as $theme) {
        $terms = taxonomy_get_term_by_name($theme, 'dkan_topics');
        if (!empty($terms)) {
          $terms = array_filter(
          $terms,
          function ($term) use ($theme) {
            return $term->name == $theme;
          }
            );

          $themes_matched = array_merge($terms, $themes_matched);
        }
      }

      if (!empty($themes_matched)) {
        $message = t("Found topics association for node @dataset_title.",
        array('@dataset_title' => $datasetEMW->label())
          );
        $this->saveMessage($message, MigrationBase::MESSAGE_INFORMATIONAL);
        $datasetEMW->field_topic->set($themes_matched);
        $datasetEMW->save();
      }
    }

    migrate_instrument_stop('MedataDatajsonHarvestMigration->complete');
  }

  /**
   * Download remote file locally.
   */
  private function downloadRemoteFile(EntityMetadataWrapper &$resourceUnsaved, stdClass $res) {
    $download_directory_path = variable_get('medata_harvest_datajson_destination_path', MEDATA_HARVEST_DATAJSON_DESTINATION_PATH);

    $success = FALSE;
    if (!file_prepare_directory($download_directory_path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      // Something went left. Report.
      $message = t(
      "Failed to prepare destination folder @destination. Skipping file download (@url)",
      array('@destination' => $download_directory_path, '@url' => $res->url)
        );
      $this->saveMessage($message);
    }
    else {
      // Destination directory ready, prepare filename and download the file.
      $parsed_url = parse_url($res->url);

      // Files naming convention is not url friendly, use urldecode.
      $filename = drupal_basename(urldecode($parsed_url['path']));
      $filename = transliteration_clean_filename($filename);

      $file_field_upload_destination = str_replace('///', '//', "{$download_directory_path}/") . $filename;

      $file_temporary_destination = "temporary://" . md5($res->url);

      $file_temporary_handler = fopen($file_temporary_destination, 'w');

      $ch = curl_init($this->uri);

      curl_setopt($ch, CURLOPT_URL, $res->url);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_TIMEOUT, 15);
      curl_setopt($ch, CURLOPT_FILE, $file_temporary_handler);

      // Start the download process.
      if (curl_exec($ch) === FALSE || $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE) >= 400) {
        // Log the error.
        $error = "";

        if ($http_code >= 400) {
          $error = "HTTP Code " . $http_code;
        }
        elseif (curl_errno($ch)) {
          $error = "ERROR - " . curl_error($ch);
        }

        $message = t("Failed to downloaded file locally from source (@url) for resource (@resource_title): @error",
        array(
          '@resource_title' => $res->title,
          '@url' => $res->url,
          '@error' => $error,
        )
          );

        $this->saveMessage($message);
      }
      else {
        // We have the file. Save it as a file object to the resource node.
        // Use FILE_EXISTS_REPLACE since files can take a lot of space and get duplicated easily.
        $file_field_upload_uri = file_unmanaged_move($file_temporary_destination, $file_field_upload_destination, FILE_EXISTS_REPLACE);

        db_delete('file_managed')
          ->condition('uri', $file_field_upload_uri)
          ->execute();
        entity_get_controller('file')
          ->resetCache();

        // Generate and save the file object.
        $file_field_upload_object = file_uri_to_object($file_field_upload_uri, FALSE);
        $file_field_upload = file_save($file_field_upload_object);

        $message = t(
        "File downloaded locally from source (@url) for resource (@resource_title).",
        array(
          '@resource_title' => $res->title,
          '@url' => $res->url,
        )
          );
        $this->saveMessage($message, MigrationBase::MESSAGE_INFORMATIONAL);

        $resourceUnsaved->field_upload = $file_field_upload;

        $success = TRUE;
      }

      // We should be done with the curl process by now.
      curl_close($ch);

      // file_unmanaged_move() will take care of removing the temporary file.
      // We just need to close the handler.
      fclose($file_temporary_handler);

      return $success;
    }
  }

  /**
   * Update datastore config for a resource and add to the queue .
   */
  private function getResourceDatastoreConfig(EntityMetadataWrapper $resourceEMW, stdClass $res) {
    $queue_add = self::DATASTORE_FAST;

    $delimiter_allowed_values = variable_get('medata_harvest_datajson_delimiter_detect_list', array(',', ';', 'TAB', '|', '+'));

    // Load allowed encodings. This depends on the mbstring extension currently
    // in use.
    $encoding_allowed_values = array();
    // From FeedsCSVParser::configEncodingForm().
    if (extension_loaded('mbstring') && variable_get('feeds_use_mbstring', TRUE)) {
      // Get the system's list of available encodings.
      $encoding_allowed_values = mb_list_encodings();
      // Make the key/values the same in the array.
      $encoding_allowed_values = array_combine($encoding_allowed_values, $encoding_allowed_values);
      // Sort alphabetically not-case sensitive.
      natcasesort($encoding_allowed_values);
    }

    $encoding_detect_list = variable_get('medata_harvest_datajson_encoding_detect_list', array('UTF-8', 'ISO-8859-3'));

    $encoding_allowed_values = array_intersect($encoding_allowed_values, $encoding_detect_list);

    // Datastore config update if available and valid.
    $datastore_feedscsvparser_config_properties = array(
      'medataDelimiter' => array(
        'feeds_config' => 'delimiter',
    // From FeedsCSVParser::getAllDelimiterTypes().
        'allowed_values' => $delimiter_allowed_values,
      ),
      'medataEncoding' => array(
        'feeds_config' => 'encoding',
        'allowed_values' => $encoding_allowed_values,
      ),
    );

    $config_updates = array();

    if (isset($res->raw)) {
      foreach ($datastore_feedscsvparser_config_properties as $property => $feeds_mapping) {
        if (!isset($res->raw->$property)) {
          $this->saveMessage(
          t("Datastore property not provided by the source: @property", array('@property' => $property))
            );
          continue;
        }

        // Source inconsistant about the naming. Search case insensitive.
        $allowed_values_matched = preg_grep("/" . $res->raw->$property . "/i", $feeds_mapping['allowed_values']);

        if (empty($allowed_values_matched)) {
          $this->saveMessage(
          t("Datastore property provided by the source not allowed: @property [@value]", array('@property' => $property, '@value' => $res->raw->$property))
            );
          continue;
        }

        // In case multipe where found, get the first value.
        $config_updates[$feeds_mapping['feeds_config']] = array_pop($allowed_values_matched);
      }
    }

    // Try to do some detection on our own to raise warning if source is wrong.
    $alternative_config_updates = array();

    $uri = $resourceEMW->field_upload->value()->uri;
    $reader = Reader::createFromPath($uri, 'r');

    /**
     * Delimiter detection.
     */
    $limit = $reader->count() >= 500 ? 500 : $reader->count();
    $result = delimiter_detect($reader, $delimiter_allowed_values, $limit);

    // Number of rows test or more means a high chance of being the actual
    // delimiter.
    $result_max = max($result);
    if ($result_max >= $limit) {
      $alternative_config_updates['delimiter'] = array_search($result_max, $result);;
      $this->saveMessage(
      t("Datastore property @property detected: \"@value\"", array('@property' => 'delimiter', '@value' => $alternative_config_updates['delimiter'])),
      MigrationBase::MESSAGE_INFORMATIONAL
        );
    }
    else {
      $this->saveMessage(t("Datastore property @property detection failed.", array('@property' => 'delimiter')), MigrationBase::MESSAGE_WARNING);
    }

    /**
     * Encoding detection.
     */
    $max_chunks = variable_get('medata_harvest_datajson_encoding_detect_max_chunks', 5);

    $encoding_detect = array();

    // Load 10 megabytes per test.
    $limit = 0;
    foreach ($reader->chunk(10485760) as $record) {
      $encoding = mb_detect_encoding($record, $encoding_detect_list);

      if (!empty($encoding)) {
        $encoding_detect[$encoding]++;
      }

      $limit++;
      if ($limit >= $max_chunks) {
        break;
      }
    }

    // Number of chunks tested or more means a high chance of the encoding detected being correct.
    $encoding_detect_max = max($encoding_detect);
    if ($encoding_detect_max == $limit) {
      $alternative_config_updates['encoding'] = array_search($encoding_detect_max, $encoding_detect);;
      $this->saveMessage(
      t("Datastore property @property detected: \"@value\"", array('@property' => 'encoding', '@value' => $alternative_config_updates['encoding'])),
      MigrationBase::MESSAGE_INFORMATIONAL
        );
    }
    else {
      $this->saveMessage(t("Datastore property @property detection failed.", array('@property' => 'encoding')), MigrationBase::MESSAGE_WARNING);
    }

    // Test sourced vs detected if possible.
    foreach ($datastore_feedscsvparser_config_properties as $property => $feeds_mapping) {
      $property = $feeds_mapping['feeds_config'];

      // Skip check if nothing was detected.
      if (empty($alternative_config_updates[$property])) {
        continue;
      }

      // Use detected if source not found.
      if (empty($config_updates[$property])) {
        $config_updates[$property] = $alternative_config_updates[$property];
      }

      // Raise error if mismatch detected.
      if ($alternative_config_updates[$property] != $config_updates[$property]) {
        $this->saveMessage(t("Datastore property @property detected (@value_detected) different then source (@value_provided): ",
        array(
          '@property' => $property,
          '@value_provided' => $config_updates[$property],
          '@value_detected' => $alternative_config_updates[$property],
        )));

        $queue_add = self::DATASTORE_SKIP;
      }
    }

    $sourceConfigUpdate = array(
      'FeedsCSVParser' => $config_updates,
    );

    /**
     * Empty Headers detection.
     * This is not part of the Datastore checks but needed for the queue_add flag.
     * Empty columns headers breaks the Datastore.
     */
    $reader->setHeaderOffset(0);
    $delimiter = $sourceConfigUpdate['FeedsCSVParser']['delimiter'];

    if ($delimiter == "TAB") {
      $delimiter = "\t";
    }

    $reader->setDelimiter($delimiter);

    $empty_headers = array();
    $lengthy_headers = array();
    $headers = $reader->getHeader();

    if (count($headers) === 1) {
      // Legitimate CSV, but red flag.
      $this->saveMessage(t("Single Column CSV."));
      $queue_add = self::DATASTORE_SKIP;
    }
    elseif (count($headers) > variable_get('medata_harvest_datajson_datastore_headers_max', 312)) {
      // A huge number of column may cause "SQLSTATE Row size too large" errors.
      $this->saveMessage(t("@count columns CSV. Use slow datastore", array('@count' => count($headers))));
      $queue_add = self::DATASTORE_SLOW;
    }

    foreach ($headers as $key => $header) {
      if (empty($header)) {
        $empty_headers[] = $key;
      }

      if (strlen($header) > 65535) {
        $lengthy_headers[] = $key;
      }
    }

    if (!empty($empty_headers)) {
      $this->saveMessage(t("The following headers indexes are empty: @indexes.", array('@indexes' => implode(',', $empty_headers))));
      $queue_add = self::DATASTORE_SKIP;
    }

    if (!empty($lengthy_headers)) {
      $this->saveMessage(t("The following headers indexes are too long: @indexes.", array('@indexes' => implode(',', $lengthy_headers))));
      $queue_add = self::DATASTORE_SKIP;
    }

    return array($queue_add, $sourceConfigUpdate);
  }

}
