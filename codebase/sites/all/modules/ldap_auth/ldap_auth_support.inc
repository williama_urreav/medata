<?php
include_once('ldap_auth_ldap_config.inc');
function ldap_auth_support($form, $form_state)
{
    $form['mo_header_style'] = array('#markup' => '<div class="mo_ldap_table_layout_1">');

    $form['mo_top_div'] = array(
        '#markup' => '<div class="mo_ldap_table_layout mo_ldap_container">',
    );

    $form['markup_support_1'] = array(
        '#markup' => '<h2><strong>Support/Feature Request:</strong></h2>Need any help? Just send us a query so we can help you.<br />',
    );

    $form['miniorange_ldap_email_address'] = array(
        '#type' => 'textfield',
        '#title' => t('Email Address'),
        '#attributes' => array('style' => 'width:70%','placeholder' => 'Enter your email'),
    );

    $form['miniorange_ldap_phone_number'] = array(
        '#type' => 'textfield',
        '#title' => t('Phone number'),
        '#attributes' => array('style' => 'width:70%','placeholder' => 'Enter your phone number'),
    );

    $form['miniorange_ldap_support_query'] = array(
        '#type' => 'textarea',
        '#title' => t('Query'),
        '#cols' => '10',
        '#rows' => '5',
        '#resizable' => false,
        '#attributes' => array('style' => 'width:70%','placeholder' => 'Write your query here'),
    );
    $form['miniorange_ldap_support_submit_click'] = array(
        '#type' => 'submit',
        '#value' => t('Submit Query'),
        '#submit' => array('send_support_query'),
        '#id' => 'button_config',
    );

    $form['miniorange_ldap_support_note'] = array(
        '#markup' => '<br><br>If you want custom features in the module, just drop an email to <a href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a></div></div>'
    );

    return $form;

    }

?>