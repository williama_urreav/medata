<?php
/**
 * @file
 * Contains Drupal Commerce Hooks.
 */

/**
 * Implements hook_commerce_checkout_pane_info().
 */
function flexiform_commerce_checkout_pane_info() {
  $panes = array();

  /** @var \FlexiformController $controller */
  $controller = entity_get_controller('flexiform');
  foreach ($controller->loadWithDisplay('FlexiformDisplayCheckoutPane') as $form) {
    $display = $form->getDisplay('FlexiformDisplayCheckoutPane');
    $panes['flexiform__'.$form->form] = array(
      'pane_id' => 'flexiform__'.$form->form,
      'title' => $display->configuration['title'],
      'name' => $display->configuration['title'],
      'review' => FALSE,
      'file' => 'flexiform.checkout.inc',
      'base' => 'flexiform_checkout_pane',
      'flexiform' => $form->form,
    );
  }
  return $panes;
}
