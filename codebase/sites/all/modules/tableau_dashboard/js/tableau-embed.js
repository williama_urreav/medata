/**
 * @file
 * Javascript file used to save custom view if user changes any filters.
 *
 * User: rok
 * Date: 26/04/2017
 * Time: 09:24.
 */

(function ($) {

  'use strict';
  Drupal.behaviors.tableau_dashboard = {
    attach: function (context, settings) {
      var dashboards = $.map(drupalSettings.tableau_dashboard.dashboards, function(value, index) {
        return [value];
      });
      for (var i = 0; i < dashboards.length; i++) {
        $.get("/tableau/ticket", function (ticket) {
          var dashboard = dashboards.shift();
          var containerDiv = document.createElement("DIV");
          $(".vizContainer[data-tableau-container=" + dashboard.containerId + "]").append(containerDiv);
          // If the Site Name is blank or is equal to default we apply the default URL structure. Otherwise we apply the Site based URL structure.
          if (!drupalSettings.tableau_dashboard.siteName || drupalSettings.tableau_dashboard.siteName == 'default') {
            var url = drupalSettings.tableau_dashboard.url + "/trusted/" + ticket + "/views/" + dashboard.display;
          } else {
            var url = drupalSettings.tableau_dashboard.url + "/trusted/" + ticket + "/t/" + drupalSettings.tableau_dashboard.siteName + "/views/" + dashboard.display;
          }
          var options = {
              hideTabs: drupalSettings.tableau_dashboard.hideTabs,
              hideToolbar: drupalSettings.tableau_dashboard.hideToolbar,
              onFirstInteractive: function () {
                viz.getWorkbook();
              }
            };

          var viz = new tableau.Viz(containerDiv, url, options);
          viz.addEventListener(tableau.TableauEventName.MARKS_SELECTION, saveState);
          viz.addEventListener(tableau.TableauEventName.FILTER_CHANGE, saveState);
          viz.addEventListener(tableau.TableauEventName.TAB_SWITCH, saveState);
          viz.addEventListener(tableau.TableauEventName.PARAMETER_VALUE_CHANGE, saveState);
          viz.addEventListener(tableau.TableauEventName.STORY_POINT_SWITCH, saveState);
        });
      }

    }
  };

  var customViewName = "customView";
  function saveState(e) {
    var workbook = e.$2.$a.$E;
    // Try to save state, or print error.
    workbook.rememberCustomViewAsync(customViewName).then(function (success) {
        workbook.setActiveCustomViewAsDefaultAsync();
      },
      function (err) {
        console.log("An error occured:");
        console.log(err);
      });
  }

  function restoreState() {
    var workbook = e.$2.$a.$E;
    // Try to retrieve state, or print error.
    workbook.showCustomViewAsync(customViewName).otherwise(function (err) {
      console.log("An error occured:");
      console.log(err);
    });
  }
})(jQuery, drupalSettings);
