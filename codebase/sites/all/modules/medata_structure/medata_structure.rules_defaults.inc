<?php

/**
 * @file
 * medata_structure.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function medata_structure_default_rules_configuration() {
  $items = array();
  $items['rules_email'] = entity_import('rules_config', '{ "rules_email" : {
      "LABEL" : "Email Notification to medata@medellin.gov.co on new Feedback content",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Email", "Feedback", "Notification" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--feedback" : { "bundle" : "feedback" } },
      "DO" : [
        { "mail" : {
            "to" : "medata@medellin.gov.co",
            "subject" : "[MEData] Comentario: \\u0022[node:title]\\u0022",
            "message" : "[node:body]\\r\\n\\r\\n[node:url]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
