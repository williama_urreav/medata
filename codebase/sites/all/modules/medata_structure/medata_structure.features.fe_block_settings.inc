<?php

/**
 * @file
 * medata_structure.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function medata_structure_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu_block-1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 1,
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'menu_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'medata' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'medata',
        'weight' => -40,
      ),
      'nuboot_radix' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'nuboot_radix',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
