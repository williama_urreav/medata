
README - Accessibility tool module
**********************************

SHORT PROJECT DESCRIPTION
--------------------------

Accessibility tool is meant to help people with visual disabilities view
content. The tool allows users to choose between pre-selected contrasts and to
zoom in and out with highly visible buttons. We have also included an optional
help button that links to your site's help page.

Features:
  - Increases or decreases pages' relative size unit.
  - Allows users to adjust contrast.
  - Adds help link.
  - Allow users with selected permissions to change settings for tool position,
    tool color, contrast color options, and URL to help page.


REQUIREMENTS
------------

  - Color
  - JavaScript enabled in your browser


USAGES
------

Add Accessibility tool block to selected region.

Add "at-contrast" or "at-alt-contrast" classes to elements apply contrast 
changes. Or use Accessibility tool settings page ( Configuration -> User 
interface -> Accessibility tool settings ) to add selectors.

Use em/rem for any sizing you would like to scale when zooming in and out 
especially font-sizes and grid max-widths.
