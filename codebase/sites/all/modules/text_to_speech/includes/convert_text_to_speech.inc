<?php

/**
 * @file
 * This file is used to convert the text to .mp3 file format.
 */

/**
 * The text_to_speech page callback function.
 */
function _text_to_speech_page_callback() {
  $dest_path = variable_get('file_public_path', drupal_realpath("public://"));
  $filename = $dest_path . "/" . md5(rand()) . ".mp3";
  $text = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';
  $output = "The filename is : " . $filename . "<br /><br />\n\n";
  $output .= "The conversion text is : " . $text . "<br /><br />\n\n";
  $output .= _text_to_speech_convert($filename, $text);
  return $output;
}

/**
 * Convert the given input text to .mp3 format audio file.
 *
 * @param string $filename
 *   Convert filename.
 * @param string $text
 *   Input text to convert.
 */
function _text_to_speech_convert($filename, $text) {
  if (file_exists($filename)) {
    unlink($filename);
  }
  $postdata = http_build_query(
    array(
      "speech" => $text,
      // Options for voice are:"nitech_us_rms_arctic_hts","nitech_us_bdl_arctic_hts","nitech_us_slt_arctic_hts","nitech_us_awb_arctic_hts".
      "voice" => "nitech_us_rms_arctic_hts",
      // Options for volume_scale are: 1 to 10.
      "volume_scale" => 5,
      "make_audio" => "Convert Text To Speech",
    )
  );
  $opts = array(
    'http' => array(
      'method' => 'POST',
      'header' => 'Content-type: application/x-www-form-urlencoded',
      'content' => $postdata,
    ),
  );
  $context = stream_context_create($opts);
  if ($htmldocwithlink = file_get_contents("http://www.text2speech.org", FALSE, $context)) {
    $htmldoc = new DOMDocument();
    $htmldoc->loadHTML($htmldocwithlink);
    $soundfilelink = $htmldoc->getElementById('downloadlink')->getElementsByTagName('a')->item(0)->getAttribute('href');
    $soundfile = file_get_contents('http://www.text2speech.org/' . $soundfilelink);
    file_put_contents($filename, $soundfile);
    return '<audio autoplay="autoplay" controls="controls"> <source src="' . $filename . '" type="audio/mp3" /></audio>';
  }
  else {
    return t("Audio could not be saved");
  }
}
