<?php

/**
 * @file
 * Theme functions.
 */

// Include all files from the includes directory.
$includes_path = dirname(__FILE__) . '/includes/*.inc';
foreach (glob($includes_path) as $filename) {
  require_once dirname(__FILE__) . '/includes/' . basename($filename);
}

// drupal_add_js($theme_path.'/js/jquery.easing.1.3.js', array('type' => 'file', 'scope' => 'footer'));.
/**
 * Implements template_preprocess_page().
 */
function medata_preprocess_page(&$variables) {
  // Inject copyright to theme. Using this way will spear the issue of excessif
  // div wrapping using the theme settings.
  if ($copyright = theme_get_setting('copyright')) {
    $variables['copyright'] = t('Copyright © <b>Medata</b> @year ®. All rights reserved', array('@year' => format_date(time(), 'custom', 'Y')));
  }

  $variables['display_login_menu'] = (theme_get_setting('display_login_menu') === NULL) ? 1 : theme_get_setting('display_login_menu');

  // Add matchHeight in Frontpage.
  drupal_add_js(drupal_get_path('theme', 'medata') . '/assets/js/jquery.matchHeight-min.js', array(
    'scope' => 'footer',
    'weight' => 0,
  ));
  drupal_add_js(drupal_get_path('theme', 'medata') . '/assets/js/tweaks.script.js', array(
    'scope' => 'footer',
    'weight' => 1,
  ));
  $vars['scripts'] = drupal_get_js();

  medata_preprocess_page_hero($variables);

  // Init the main_classes_array template variable.
  $variables['main_classes_array'] = array();

  medata_preprocess_page_data_dashboard($variables);

  medata_preprocess_page_kibana($variables);
}

/**
 *
 */
function medata_preprocess_page_hero(&$variables) {
  // Add Hero section to select number of pages.
  $hero_enabled_path_patterns = array(
    'search*' => array(
      'title' => t('Buscar datos'),
      'description' => t("Search, exploration, and download of data sets of the Medellín City Hall."),
    ),
    'análisis-de-datos*' => array(
      'title' => t('Análisis de datos'),
      'description' => t("Crossing and displaying indicators using different data sources."),
    ),
    'medellín-en-cifras*' => array(
      'title' => t('Medellín en cifras'),
      'description' => t("Dynamic dashboards to explore the most important figures of the city."),
    ),
    'historia*' => array(
      'title' => t('Historias de datos'),
      'description' => t("Notes, narratives and stories related to Medellín's data."),
    ),
  );

  $variables['hero_enabled'] = FALSE;
  foreach ($hero_enabled_path_patterns as $pattern => $pattern_info) {
    if (drupal_match_path(drupal_get_path_alias(), $pattern)) {
      $variables['hero_info'] = $pattern_info;
      $variables['hero_enabled'] = TRUE;
      return;
    }
  }
}

/**
 *
 */
function medata_preprocess_page_kibana(&$variables) {
  if (drupal_match_path(drupal_get_path_alias(), 'análisis-de-datos')) {
    // Make the kibana display take the full viewport.
    $variables['main_classes_array'] = array('main', 'container-fluid');

    $variables['breadcrumb'] = array();
  }
}

/**
 *
 */
function medata_preprocess_page_data_dashboard(&$variables) {
  if (isset($variables['node']->type)
    && $variables['node']->type == 'data_dashboard'
    && isset($variables['node']->panelizer_view_mode)
    && $variables['node']->panelizer_view_mode == 'default') {
    // Make the data-dashboard display take the full viewport.
    $variables['main_classes_array'] = array('main', 'container-fluid');


    $variables['breadcrumb'] = array();
  }
}

/**
 * Implements hook_form_alter().
 */
function medata_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'colorizer_admin_settings':
      $form['colorizer_global']['colorizer_cssfile']['#default_value'] = 'colorizer/colorizer.css';
      $form['colorizer_global']['colorizer_incfile']['#default_value'] = 'colorizer/colorizer.inc';
      break;
  }
}

/**
 *
 */
function medata_css_alter(&$css) {
  foreach (array_keys($css) as $key) {
    if (strpos($key, 'themes/nuboot_radix/assets/css') !== FALSE) {
      unset($css[$key]);
    }

  }
}

/**
 * Implements theme_menu_tree__menu_block().
 */
function medata_menu_tree__menu_block(&$variables) {
  if ($node->type == 'page') {
    return '<ul class="menu">' . $variables['tree'] . '</ul>';
  }

  return '<ul class="menu nav nav-pills nav-stacked">' . $variables['tree'] . '</ul>';
}
