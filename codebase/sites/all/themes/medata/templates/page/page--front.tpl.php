<?php

/**
 * @file
 * Front page - no container class in main section.
 */
?>
<header id="header" class="header">
  <nav class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header pull-right">
        <?php if ($logo): ?>
          <a class="logo navbar-btn pull-left" href="<?php print url($front_page); ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target="#navbar-collapse">
          <span class="sr-only">Menu</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div id="navbar-collapse" class="navbar-collapse collapse menu-collapse">
        <?php if ($main_menu): ?>
          <ul id="main-menu" class="menu nav navbar-nav">
            <?php print render($main_menu); ?>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </nav>
</header>
<!-- Hero Section -->
<div class="hero">
  <div class="container">
    <div class="search-quote col-md-4 col-sm-12">
      <?php
        print t("<span class='search-over'>Search over</span> <span class='dataset-number'>@count datasets</span>", array('@count' => medata_structure_metric_datasets_count()))
      ?>
    </div>
    <div class="search-input col-md-6 col-sm-12">
      <!-- views exposed search -->
      <?php
      $block = block_load('dkan_sitewide', 'dkan_sitewide_search_bar');
      if($block):
        $search = _block_get_renderable_array(_block_render_blocks(array($block)));
        print render($search);
      endif;
      ?>
    </div>
    <div class="hero-logo col-md-2 hidden-sm">
      <img src="<?php print '/' . drupal_get_path('theme', 'medata') . "/assets/images/medata-logo.png" ?>" alt="medata logo">
    </div>
  </div>
</div>
<div class="clearfix"></div>

<div id="main-wrapper">
  <div id="main" class="main">

    <?php print $messages; ?>
    <?php if (!empty($page['help'])): ?>
      <?php print render($page['help']); ?>
    <?php endif; ?>


    <div class="main-row">

      <section>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if (!empty($title) && (arg(0) == 'admin' || arg(1) == 'add' || arg(1) == 'edit')): ?>
          <h1 class="page-header"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if (!empty($tabs)): ?>
          <?php print render($tabs); ?>
        <?php endif; ?>
        <?php if (!empty($action_links)): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>
      </section>

    </div>

  </div> <!-- /#main -->
</div> <!-- /#main-wrapper -->

<footer id="footer" class="footer">
  <div class="widgets">
    <div class="pattern">
      <div class="container">
        <div class="col-md-5">
          <div class="widget widget-social">
            <h4>Sobre nosotros</h4>
            <p><b>MEData</b> es la estrategia de datos de la ciudad de Medellín,
              que busca la apropiación, apertura y uso de los datos como herramienta de gobierno,
              acción ciudadana y toma de decisiones.
            </p>
            <div class="social-icons">
              <ul class="inline">
                <li><a href="https://www.facebook.com/AlcaldiadeMed" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/AlcaldiadeMed" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.instagram.com/alcaldiademed/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UCAFLU70jPz4bODq_5BSWPIg" target="_blank"><i class="fa fa-youtube"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="widget widget-links">
            <h4>Otros links</h4>
            <div class="widget-menu-list">
              <ul class="list-group">
                <li class="list-group-item">
                  <a href="https://www.medellin.gov.co/irj/portal/medellin" target="_blank">
                    Portal Alcaldía de Medellín
                  </a>
                </li>
                <li class="list-group-item">
                  <a href="https://www.rutanmedellin.org/es/" target="_blank">
                    Página Corporación Ruta N
                  </a>
                </li>
                <li class="list-group-item">
                  <a href="https://www.datos.gov.co/" target="_blank">
                    Potal Datos Abiertos Colombia
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="widget widget-contact">
            <h4>Contáctenos</h4>
            <ul class="fa-ul">
              <li>
                <i class="fa-li fa fa-map-pin"></i>
                Calle 44 N 52 – 165 Centro Administrativo la Alpujarra – Medellín, Colombia.
              </li>
              <li><i class="fa-li fa fa-send"></i>medata@medellin.gov.co</li>
              <li><i class="fa-li fa fa-phone"></i>Línea única de atención ciudadana: (574) 44 44 144</li>
              <li><i class="fa-li fa fa-phone"></i>Línea gratuita nacional: 01 8000 411 144</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyrights">
    <div class="copyright-wrap">
      <?php if ($copyright): ?>
        <small class="copyright"><?php print $copyright; ?></small>
      <?php endif; ?>
      <span class="seperator">|</span>
      <small><a href="/condiciones-de-uso">Condiciones de Uso</a></small>
      <span class="seperator">|</span>
      <small><a href="/privacidad">Políticas de Privacidad</a></small>
    </div>
  </div>
</footer>
