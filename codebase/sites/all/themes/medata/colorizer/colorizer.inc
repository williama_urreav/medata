<?php
/**
 * @file
 * Colorizer options.
 */

$info = array(
  // Available colors and color labels used in theme.
  'fields' => array(
    'top' => t('Primary color'),
    'bottom' => t('Secondary color'),
    'toolbarbtn' => t('Menu bar background'),
    'toolbarborder' => t('Border color'),
    'bg' => t('Main background color'),
    'sidebar' => t('Footer text color'),
    'sidebarborders' => t('Footer link color'),
    'footer' => t('Footer background color'),
    'titleslogan' => t('Site name and slogan'),
    'herotitle' => t('Hero Title'),
    'title' => t('Title color'),
    'text' => t('Text color'),
    'link' => t('Link color'),
    'linkactive' => t('Link (active)'),
    'linkhover' => t('Link (hover)'),
  ),
  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Medata'),
      'base' => '#eeeeee',
      'colors' => array(
        'top' => '#00adee',
        'bottom' => '#603476',
        'toolbarbtn' => '#713C98',
        'toolbarborder' => '#CDCDCD',
        'bg' => '#FFFFFF',
        'sidebar' => '#3B3B3B',
        'sidebarborders' => '#00adee',
        'footer' => '#ffffff',
        'titleslogan' => '#3B3B3B',
        'herotitle' => '#3B3B3B',
        'title' => '#3B3B3B',
        'text' => '#3B3B3B',
        'link' => '#00adee',
        'linkactive' => '#603476',
        'linkhover' => '#603476',
      ),
    ),
  ),

  // Gradient definitions.
  'gradients' => array(
    array(
      // (x, y, width, height).
      'dimension' => array(0, 0, 0, 0),
      // Direction of gradient ('vertical' or 'horizontal').
      'direction' => 'vertical',
      // Keys of colors to use for the gradient.
      'colors' => array('top', 'bottom'),
    ),
  ),

);
