<?php
/**
 * @file
 * Template for Medata layout.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display panel-medata clearfix <?php if (!empty($classes)) { print $classes; } ?><?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <div class="container-fluid">
    <div class="row panel-page-links">
        <div class="panel-panel-inner">
          <?php print $content['row-1']; ?>
        </div>
    </div>
    <div class="row panel-data-analysis">
        <div class="panel-panel-inner">
          <?php print $content['row-2']; ?>
        </div>
    </div>
    <div class="row panel-widgets">
        <div class="panel-panel-inner">
          <?php print $content['row-3']; ?>
        </div>
    </div>
    <div class="row panel-topics">
        <div class="panel-panel-inner">
          <?php print $content['row-4']; ?>
        </div>
    </div>
    <div class="row panel-stories">
        <div class="panel-panel-inner">
          <?php print $content['row-5']; ?>
        </div>
    </div>
  </div>
</div><!-- /.medata -->
