<?php

/**
 * @file
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Medata layout'),
  'icon' => 'medata-layout.png',
  'category' => t('Custom'),
  'theme' => 'medata',
  'css' => 'medata-layout.css',
  'regions' => array(
    'row-1' => t('Content panel page links'),
    'row-2' => t('Content panel data analysis'),
    'row-3' => t('Content panel widgets'),
    'row-4' => t('Content panel topics'),
    'row-5' => t('Content panel stories'),
  ),
);
