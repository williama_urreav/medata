/**
 * Created by noomane on 26/10/17.
 */
/**
 * @file
 * Custom scripts for theme.
 */
(function ($) {

  var options = {
    property: 'height'
  };

  $(".d-a-item").matchHeight(options);

  var changemargin = function(){
    var width = jQuery('ul.menu > li.leaf > a').width();
    $('ul.menu > li.leaf > a::before').css('margin-left', (width/2)+'px'); // use px
  };

  // run function here
  //changemargin();
  // run it again on resize
  //$(window).on('resize',changemargin);

})(jQuery);

