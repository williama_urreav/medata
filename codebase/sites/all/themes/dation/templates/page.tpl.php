<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <?php if ($logo): ?>
	    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
	  <?php endif; ?>

    <nav class="navbar navbar-static-top">
      <!--a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a-->

        <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu custom-section-left">
        <?php print render($page['header_left']); ?>
        <?php print $site_name; ?>
      </div>
      <div class="navbar-custom-menu custom-section-middle">
      <?php print render($page['header_center']); ?> 
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> 
    </div>
      <div class="navbar-custom-menu custom-section-right">
      <?php print render($page['header_right']); ?>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <?php if (!empty($lte_examples)) {?>
          <?php include_once($themepath . '/examples/header-menu.expl'); ?>
          <?php include_once($themepath . '/examples/header-user.expl'); ?>
          <?php } else { ?>
          
          <?php if (!empty($page['header'])) { ?>
              <?php foreach ($page['header'] as $values) { ?>
                <?php if (empty($values['#block'])) { continue; } ?>
                <li class="nav-item px-3">
                  <?php print render($values); ?>
                </li>
              <?php } ?>
          <?php } ?>
                
          <?php if (!empty($page['navigation'])) { ?>
            <?php foreach ($page['navigation'] as $values) { ?>
              <?php if (empty($values['#block'])) { continue; } ?>
              <li class="nav-item navigation">
                <?php print render($values); ?>
              </li>
            <?php } ?>
          <?php } ?>
              
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <?php if (!empty($auth_user)) { ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <?php echo $auth_user['user_picture']; ?>
                      <span class="hidden-xs"><?php echo $auth_user['name']; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header">
                        <?php echo $auth_user['user_picture']; ?>

                        <p>
                          <?php echo $auth_user['name']; ?>
                          <small><?php echo t('Member since @month', ['@month' => date('F Y', $auth_user['created'])]); ?></small>
                        </p>
                      </li>
                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left">
                          <a href="<?php echo (!empty($cleanurl) ? base_path() : '?q=') . 'user'; ?>" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                          <a href="<?php echo (!empty($cleanurl) ? base_path() : '?q=') . 'user/logout'; ?>" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  </li>

                <?php } ?>
          <?php } ?>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
      </div>
    </nav>

  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <?php print render($page['sidebar_first']); ?>
	</section>
	<!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 100px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if ($breadcrumb): ?>
        <ol class="breadcrumb"><li><?php print $breadcrumb; ?></li></ol>
      <?php endif; ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
      <?php if ($messages): ?>
        <div id="messages">
          <div class="section clearfix">
            <?php print $messages; ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <?php print render($page['content_top_first']); ?>
        </div>
        <div class="col-lg-3 col-xs-6">
          <?php print render($page['content_top_second']); ?>
        </div>
        <div class="col-lg-3 col-xs-6">
          <?php print render($page['content_top_third']); ?>
        </div>
        <div class="col-lg-3 col-xs-6">
          <?php print render($page['content_top_fourth']); ?>
        </div>
      </div>

      <!--main page cont -->
      <div class="row">
       <?php if($page['sidebar_second']) { $primary_col = 7; } else { $primary_col = 12; } ?>
         <section class="col-lg-<?php print $primary_col; ?>">
		 <div class="box box-success"><div class="box-body">
           <?php if ($tabs): ?>
             <div class="tabs">
               <?php print render($tabs); ?>
             </div>
           <?php endif; ?>
           <?php print render($page['content']); ?>
		  </div></div>
        </section>
       <!-- /.col -->
       <!--sidebar_second -->
       <?php if($page['sidebar_second']) :?>
	   <section class="col-lg-5">
         <div class="box box-success"><div class="box-body">
		 <?php print render($page['sidebar_second']); ?>
		 </div></div>
       </section>
	   <?php endif; ?>

      </div>

    </section>
	
	<div class="row">
       <?php if($page['content_left'] || $page['content_right']): ?>
	   <div class="col-md-6">
	     <?php if($page['content_left']): ?>
		   <div class="box box-widget">
		    <div class="box box-body">
			   <?php print render($page['content_left']); ?>
			</div>
           </div>
		 <?php endif; ?>
	   </div>
       <div class="col-md-6">
	     <?php if($page['content_right']): ?>
		   <div class="box box-widget">
		    <div class="box box-body">
			   <?php print render($page['content_right']); ?>
			</div>
           </div>
		 <?php endif; ?>
	   </div>
	   <?php endif; ?>
    </div>

  </div>
     <!-- /.content-wrapper -->
     <footer class="sticky-footer">
            <div class="container">
               
                  
               <div class="row">
		<div class="col-md-4 align-self-center">
		<a class="text-white" href="https://www.medellin.gov.co"><img class="img-fluid" src="<?php print base_path(). path_to_theme(); ?>/img/logo-alcaldia-footer.png"/>
		</div>
		<div class="col-md-4 align-self-center">
		<a class="text-white" href="https://www.gov.co"><img class="img-fluid" src="<?php print base_path(). path_to_theme(); ?>/img/logo-gov.png"/>
		
		</div>
	</div>
            </div>
         </footer>
  <footer class="main-footer">
     
     <div class="pull-right hidden-xs">
       <b>Designed by:</b> <a href="https://dation.co">Dation</a>
     </div>
     <strong>Copyright &copy; <?php print date('Y'); ?> <?php print $site_name; ?>.</strong> Reservados todos los derechos
  </footer>


   <aside class="control-sidebar control-sidebar-dark">
     <?php print render($page['control_sidebar']); ?>
   </aside>
   <!-- /.control-sidebar -->
   <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
   <div class="control-sidebar-bg"></div>
</div>
 <!-- ./wrapper -->
