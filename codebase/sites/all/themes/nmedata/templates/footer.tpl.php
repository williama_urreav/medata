<footer class="sticky-footer">
            <div class="container">
            <?php if (!empty($page['footer'])) { ?>
    <?php $i = 0; ?>
    <?php foreach ($page['footer'] as $keys => $values) { ?>
      <?php if (!empty($values['#block'])) { ?>
        <?php if (!empty($values['#theme'])) { ?>
          <?php print render($values['#theme']); ?>
        <?php } else { ?>
          <?php if (!empty($values['#markup'])) { ?>
            <?php if (!empty($i)) { ?>
              &nbsp;|&nbsp;
            <?php } ?>
            <?php print($values['#markup']); ?>
            <?php $i++; ?>
          <?php } ?>
        <?php } ?>
      <?php } ?>
    <?php } ?>
    <?php unset($i); ?>
  <?php } else { ?>
               <div class="row">
		<div class="col-md-4 align-self-center">
		<a class="text-white" href="https://www.medellin.gov.co"><img class="img-fluid" src="<?php print base_path(). path_to_theme(); ?>/img/logo-alcaldia-footer.png"/>
		</div>
		<div class="col-md-4 align-self-center">
		<a class="text-white" href="https://www.gov.co"><img class="img-fluid" src="<?php print base_path(). path_to_theme(); ?>/img/logo-gov.png"/>
    <?php } ?>
		</div>
	</div>
            </div>
         </footer>
