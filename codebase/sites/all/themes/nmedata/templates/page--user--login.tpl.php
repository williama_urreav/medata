<div class="wrapper">
  
<div class="container-fluid banner-cont">
         <div class="row">
            <div class="col-lg-7 col-md-6 d-none d-md-block pl-0 pr-0">
               <div class="banner"></div>
            </div>
            <div class="col-lg-5 col-md-6 align-self-center p-3">
               <div class="mx-auto f-login">
                  </br>
                  </br>
                  </br>
                  </br>
                  <div class="text-center"><a href="<?php print $front_page; ?>"><img class="img-fluid logo-alcaldia-login" src="<?php print base_path(). path_to_theme(); ?>/img/logo_alcaldia.png"/></a></div>
                  <div class="text-center mb-4"><span class="secretaria"><?php if ($messages): ?>
                    <div id="messages">
                      <div class="section clearfix">
                      <?php print $messages; ?>
                      </div>
                    </div>
                    <?php endif; ?></span></div>
                  <div class="text-left mb-3"><span class="texto-login">Ingrese sus datos para iniciar sesión</span></div>
                  <form class="formulario">
                     <div class="form-group">
                     <?php
                        $form = drupal_get_form('user_login');
                                print drupal_render($form);
                      ?>
                     </div>
                     <!--div class="text-right">
                        <a href="login.html">
                        <button class="btn btn-login"> INGRESAR</button>
                        </a>
                        <hr/>
                     </div-->
                     <div class="recuperar-contrasena"><span></span> <a href="<?php print $front_page; ?>/user/password"><?php print t('¿Olvidó su contraseña?'); ?></a><br></div>
                  </form>
               </div>
            </div>
         </div>
      </div>
  <!-- Content Wrapper. Contains page content -->
  <!--div class="login-box">
  <div class="login-logo">
    
  </div>
  <!-- /.login-logo -->
  <!--div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    

   
    
    
    <a href="<?php //print $front_page; ?>/user/register" class="text-center register-link">Register</a>

  </div>
  <!-- /.login-box-body -->
</div>
     <!-- /.content-wrapper -->

 
</div>
 <!-- ./wrapper -->
