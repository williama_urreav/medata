  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo url('<front>'); ?>" class="logo">
    <span class="logo-mini"><?php  print substr($site_name, 0, 3) ;  ?></span>
        <!-- logo for regular state and mobile devices -->
        <span class=" admin logo-lg">
          <?php if($logo) {  ?>
                    <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            <?php }
                  else {
                   print substr($site_name, 0, 3) ;
                  }?>
        </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    <div class="custom-menu left"> 
    <h3 class="title"><?php print $site_name; ?></h3>  
    <!-- search form -->
         <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <?php if (!empty($lte_examples)) {?>
          <?php include_once($themepath . '/examples/header-menu.expl'); ?>
          <?php include_once($themepath . '/examples/header-user.expl'); ?>
          <?php } else { ?>
          
          <?php if (!empty($page['header'])) { ?>
              <?php foreach ($page['header'] as $values) { ?>
                <?php if (empty($values['#block'])) { continue; } ?>
                <li class="nav-item px-3">
                  <?php print render($values); ?>
                </li>
              <?php } ?>
          <?php } ?>
                
          <?php if (!empty($page['navigation'])) { ?>
            <?php foreach ($page['navigation'] as $values) { ?>
              <?php if (empty($values['#block'])) { continue; } ?>
              <li class="nav-item navigation">
                <?php print render($values); ?>
              </li>
            <?php } ?>
          <?php } ?>
              
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <?php if (!empty($auth_user)) { ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <?php echo $auth_user['user_picture']; ?>
                      <span class="hidden-xs"><?php echo $auth_user['name']; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header">
                        <?php echo $auth_user['user_picture']; ?>

                        <p>
                          <?php echo $auth_user['name']; ?>
                          <small><?php echo t('Member since @month', ['@month' => date('F Y', $auth_user['created'])]); ?></small>
                        </p>
                      </li>
                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left">
                          <a href="<?php echo (!empty($cleanurl) ? base_path() : '?q=') . 'user'; ?>" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                          <a href="<?php echo (!empty($cleanurl) ? base_path() : '?q=') . 'user/logout'; ?>" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  </li>

                <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </nav>
  </header>
