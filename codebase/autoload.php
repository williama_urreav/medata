<?php

/**
 * @file
 * Auto-generated autoloading class map.
 *
 * @see AutoloadCache::updateFile()
 */

return array(
  'AutoloadTests\\PSR4' => array(
    'file' => 'profiles/dkan/modules/contrib/autoload/tests/autoload_test_custom/psr-4-single-level-namespace/PSR4.inc',
    'provider' => 'autoload_test_custom',
    'namespace' => 'AutoloadTests\\PSR4',
  ),
  'Autoload\\Tests\\Example\\Test' => array(
    'file' => 'profiles/dkan/modules/contrib/autoload/tests/autoload_test_custom/tests/Test.inc',
    'provider' => 'autoload_test_custom',
    'namespace' => 'Autoload\\Tests\\Example\\Test',
  ),
  'Autoload\\Tests\\PSR0' => array(
    'file' => 'profiles/dkan/modules/contrib/autoload/tests/autoload_test_custom/psr-0/Autoload/Tests/PSR0.inc',
    'provider' => 'autoload_test_custom',
    'namespace' => 'Autoload\\Tests\\PSR0',
  ),
  'Autoload\\Tests\\PSR4' => array(
    'file' => 'profiles/dkan/modules/contrib/autoload/tests/autoload_test_custom/psr-4/PSR4.inc',
    'provider' => 'autoload_test_custom',
    'namespace' => 'Autoload\\Tests\\PSR4',
  ),
  'Autoload\\WrongNamespace\\WrongNamespace' => array(
    'file' => 'profiles/dkan/modules/contrib/autoload/tests/autoload_test_custom/wrong-namespace/WrongNamespace.php',
    'provider' => 'autoload_test_custom',
    'namespace' => 'Autoload\\WrongNamespace\\WrongNamespace',
  ),
  'Drupal\\autoload_test_drupal\\PSR0' => array(
    'file' => 'profiles/dkan/modules/contrib/autoload/tests/autoload_test_drupal/lib/Drupal/autoload_test_drupal/PSR0.php',
    'provider' => 'autoload_test_drupal',
    'namespace' => 'Drupal\\autoload_test_drupal\\PSR0',
  ),
  'Drupal\\autoload_test_drupal\\PSR4' => array(
    'file' => 'profiles/dkan/modules/contrib/autoload/tests/autoload_test_drupal/src/PSR4.inc',
    'provider' => 'autoload_test_drupal',
    'namespace' => 'Drupal\\autoload_test_drupal\\PSR4',
  ),
  'Drupal\\autoload_test_entity_ui\\ViewsController' => array(
    'file' => 'profiles/dkan/modules/contrib/autoload/tests/autoload_test_entity/modules/autoload_test_entity_ui/src/ViewsController.php',
    'provider' => 'autoload_test_entity_ui',
    'namespace' => 'Drupal\\autoload_test_entity_ui\\ViewsController',
  ),
);
